event_registry_tagger
=====================

An HTML5 interface for tagging JSON objects of the following structure:

{'events': {'eventId': {'articles': [ 
	'body': '',
    'sourceUri': '',
    'title': '',
    'sourceTitle': '',
    'excerpt': '',
    'uri': '',
    'sourceId': '',
    'repr_title': '',
    'time': '',
    'date': '',
    'sim': 0
 ]}}}

## Set up a working environment

1. Get Python 2.7, [pip](https://pip.pypa.io/en/latest/installing.html), and [virtualenv](http://virtualenv.readthedocs.org/en/latest/installation.html):
2. Set up a new virtual environment and switch to it:
	1. `virtualenv eventtagger`
	2. `cd eventtagger/bin && source activate`

3. Go to the extracted or git-cloned project folder and run: `pip install -r requirements.txt`
4. To start the server, run `python manage.py runserver 127.0.0.1:<port>` and access the interface at `http://127.0.0.1:<port>`.
5. Read the instructions and send feedback
