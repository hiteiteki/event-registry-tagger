$(document).ready(function () {
    /* Functionality that clears all checkboxes for a given event when the
     * Pocisti button is pressed next to it.                               */
    $('.btn-danger').click(function() {
        var currId;
        $(this).each(function(idx, el) {
            currId = el.id;
        });

        var strSelector = 'input[name=\"' + currId + '\"]';
        $(strSelector).each(function(i, el) {
            el.checked = false;
        });
    });

    /* Disable Oddaj button when clicked */
    $('#oddaj').click(function() {
        $(this).addClass('disabled');
    });
});
