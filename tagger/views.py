from django.shortcuts import render
from django.template import RequestContext

import json
import re
import pickle

import modifier

spl = re.compile(r'''\n+''')


# Create your views here.
def index(request):
    if request.method == 'POST':
        # Save response into pickle (for backup)
        with open('response_bkp.pkl', 'w') as f:
            pickle.dump(dict(request.POST), f)
            f.close()
        # Modify the existing 'dataset.json'
        modifier.run(content=dict(request.POST))

    # Open saved dataset
    with open('dataset.json') as f:
        js = json.load(f)
        f.close()

    events = {}
    for ev in js.get('events').iteritems():
        events[ev[0]] = []
        i = 0
        for art in ev[1].get('articles'):
            exc_idx = ''
            # list less than 20 article titles
            if i == 20:
                break
            # list excerpts for only the first 5 articles
            if i < 5:
                excerpts = list(enumerate(re.split(spl, art['body'])))[:5]
            else:
                excerpts = []

            if len(art.get('excerpt')) > 0:
                exc_idx = int(art.get('excerpt'))
            article = {'title': art.get('title'), 'uri': art.get('uri'),
                       'excerpts': excerpts, 'checked': exc_idx,
                       'repr_title': art.get('repr_title'),
                       'sim': art.get('sim')}
            events[ev[0]].append(article)
            i += 1

    response = {'events': events}

    return render(request, 'index.html', response,
                  context_instance=RequestContext(request))
