#!/usr/bin/env python
import json


def run(content=None):
    # Load pickle if one needs to save a crisis
    # import pickle
    # with open('response_bkp.pkl') as f:
    #     resp = pickle.load(f)
    #     f.close()

    # Move responses to lists
    titles = []
    excerpts = []
    for x in content.iteritems():
        if x[0][0].isdigit():
            for art in x[1]:
                if '|---|' in art:
                    uri, idx = art.split('|---|')
                    excerpts.append((x[0], uri, idx))
                else:
                    titles.append((x[0], art))

    # Open clean dataset
    with open('dataset_clean.json') as f:
        js = json.load(f)
        f.close()

    # Insert checks
    # Titles
    for x in titles:
        i = 0
        for art in js.get('events').get(x[0]).get('articles'):
            if art.get('uri') == x[1]:
                js['events'][x[0]]['articles'][i]['repr_title'] = 'yes'
                break
            i += 1

    # Excerpts
    checked = []
    for x in excerpts:
        i = 0
        for art in js.get('events').get(x[0]).get('articles'):
            uri = art.get('uri')
            if uri == x[1] and (x[0], uri) not in checked:
                js['events'][x[0]]['articles'][i]['excerpt'] = x[2]
                checked.append((x[0], uri))
                break
            i += 1

    # Save dataset
    with open('dataset.json', 'w') as f:
        json.dump(js, f)
        f.close()
